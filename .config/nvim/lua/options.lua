local set = vim.opt
set.termguicolors = false
set.fileencoding  = "utf-8"
set.fileformat    = "unix"
set.mouse         = "a"
set.lcs           = "tab::-],trail:-,nbsp:+,space:·"

set.guifont = "Iosevka Fixed Slab:h15"

set.cinoptions = ":0,l1"

-- Line numbers
set.nu            = true
set.rnu           = true
set.numberwidth   = 4

-- Case insensitive search
set.ignorecase    = true

vim.api.nvim_create_user_command("DeleteLeadingWhitespaces", ":%s/[ \t]*$//", {})
