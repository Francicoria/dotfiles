local g = vim.g

g['fourcolors#guiWarm']    = '#884477'
g['fourcolors#ctermWarm']  = 214

g['fourcolors#guiHot']     = '#F08080'
g['fourcolors#ctermHot']   = 134
-- pog
g['fourcolors#guiChill']   = '#773333'
g['fourcolors#ctermChill'] = 124

g['fourcolors#guiCold']    = '#FFA500'
g['fourcolors#ctermCold']  = 214
