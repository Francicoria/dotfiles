local map = vim.api.nvim_set_keymap

-- Disables macros
map('n', 'q', '<Nop>', {})
map('n', 'gQ', '<Nop>', {})
map('n', 'Q', '<Nop>', {})

-- Toggle whitespaces as characters
map('n', '<F2>', ':set invlist<CR>', {})
map('i', '<F2>', '<ESC>:set invlist<CR>a', {})

-- Toggle line numbers
map('n', '<F3>', ':set number!<CR>:set relativenumber!<CR>', {})
map('i', '<F3>', '<ESC>:set number!<CR>:set relativenumber!<CR>a', {})

-- Move up and down a line
map('n', '<M-j>', ':m +1<CR>', {})
map('n', '<M-k>', ':m -2<CR>', {})

-- Duplicate line down
map('n', '<M-m>', ':t.<CR>', {})
