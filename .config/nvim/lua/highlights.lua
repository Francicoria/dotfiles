local highlight = vim.api.nvim_set_hl
highlight(0, 'Statusline', { cterm={bold=true}, ctermfg=0, ctermbg=4 })
highlight(0, 'Whitespace', { ctermfg=7 })

highlight(0, 'Comment',     { cterm={italic=true}, ctermfg=9 })
highlight(0, 'Preproc',     { ctermfg=3 })
highlight(0, 'Type',        { cterm={bold=true}, ctermfg=7 })
highlight(0, 'Statement',   { cterm={bold=true}, ctermfg=3 })
highlight(0, 'Constant',    { ctermfg=5 })
highlight(0, 'String',      { ctermfg=2 })
highlight(0, 'Character',   { ctermfg=2 })
highlight(0, 'SpecialChar', { ctermfg=2 })

highlight(0, 'Visual',          { cterm={italic=true}, ctermfg=0, ctermbg=14 })
highlight(0, 'LineNr',          { cterm={italic=true}, ctermfg=7 })
highlight(0, 'LineNrAbove',     { cterm={italic=true}, ctermfg=240 })
highlight(0, 'LineNrBelow',     { cterm={italic=true}, ctermfg=240 })
highlight(0, 'ExtraWhitespace', { ctermbg=1 })
