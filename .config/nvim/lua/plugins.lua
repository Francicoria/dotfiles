local Plug = vim.fn['plug#']
vim.call('plug#begin', '~/.local/share/nvim/plugged')

Plug 'ap/vim-css-color'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-abolish'
Plug 'pix/vim-align'
Plug 'LunarWatcher/auto-pairs'
Plug 'neomake/neomake'
Plug 'kaarmu/typst.vim'
Plug 'tikhomirov/vim-glsl'

vim.call('plug#end')
