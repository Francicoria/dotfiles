local highlight = vim.api.nvim_set_hl
local blue  = "#6d65ff"
local black = "#1a1a1a"
local gray  = "#666666"
local darker_gray = "#444444"
local red     = "#e2491a"
local yellow  = "#fab519"
local magenta = "#b469ae"
local green   = "#98e024"
local cyan    = "#58d1eb"

highlight(0, 'Statusline', { bold=true, fg=black, bg=blue })
highlight(0, 'Whitespace', { fg=gray })

highlight(0, 'Comment',     { italic=true, fg=red })
highlight(0, 'Preproc',     { fg=yellow })
highlight(0, 'Type',        { bold=true, fg=gray })
highlight(0, 'Statement',   { bold=true, fg=yellow })
highlight(0, 'Constant',    { fg=magenta })
highlight(0, 'String',      { fg=green })
highlight(0, 'Character',   { fg=green })
highlight(0, 'SpecialChar', { fg=green })

highlight(0, 'Visual',          { italic=true, fg=black, bg=cyan })
highlight(0, 'LineNr',          { italic=true, fg=gray })
highlight(0, 'LineNrAbove',     { italic=true, fg=darker_gray })
highlight(0, 'LineNrBelow',     { italic=true, fg=darker_gray })
highlight(0, 'ExtraWhitespace', { bg=red })
