require("plugins")
require("options")
require("keymaps")
if vim.g.neovide then
	require("highlights-gui")
else
	require("highlights")
end
require("statusline")
